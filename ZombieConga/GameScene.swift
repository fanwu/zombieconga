//
//  GameScene.swift
//  ZombieConga
//
//  Created by 范武 on 16/4/6.
//  Copyright (c) 2016年 wupher. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
    let playableRect : CGRect
    let zombie = SKSpriteNode(imageNamed: "zombie1")
    var lastUpdateTime:NSTimeInterval = 0
    var dt:NSTimeInterval = 0
    
    let zombieMovePointsPerSec:CGFloat = 480.0
    var velocity = CGPoint.zero
    
    var lastTouchLocation:CGPoint?
    
    let zombieRotateRadiansPerSec:CGFloat = 4.0 * π
    
    override init(size: CGSize) {
        let maxAspectRatio:CGFloat = 16/9.0
        let playableHeight = size.width / maxAspectRatio
        let playableMargin = (size.height - playableHeight) / 2.0
        playableRect = CGRect(x:0, y:playableMargin, width: size.width, height: playableHeight)
        super.init(size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToView(view: SKView) {
        backgroundColor = SKColor.blackColor()
        let background = SKSpriteNode(imageNamed: "background1")
        addChild(background)
        background.position = CGPoint(x: size.width/2, y: size.height/2)
        
        
        zombie.setScale(1.0)
        zombie.position = CGPoint(x: 300, y: 300)
        zombie.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        addChild(zombie)
        
        debugDrawPlayableArea()
    }
    
    override func update(currentTime: NSTimeInterval) {
        if lastUpdateTime > 0 {
            dt = currentTime - lastUpdateTime
        }else{
            dt = 0
        }
        
        
        //End to the Touch Point
        if let lastTouchLoc = lastTouchLocation{
            let diffPoint:CGPoint = zombie.position - lastTouchLoc
            let diffLen = diffPoint.length()
            print("difflen: \(diffLen), dt: \(dt)")
            let amountToMove = velocity * CGFloat(dt)
            if diffLen <= amountToMove.length() {
                zombie.position = lastTouchLoc
                velocity = CGPoint.zero
                lastUpdateTime = currentTime
                return
            }
        }
        
        
        lastUpdateTime = currentTime


        moveSprite(zombie, velocity: velocity)
        boundsCheckZombie()
        rotateSprite(zombie, direction: velocity)
    }
    
    func moveSprite(sprite: SKSpriteNode, velocity:CGPoint) -> Void {
        let amountToMove = velocity * CGFloat(dt)
        sprite.position += amountToMove
    }
    
    func moveZombieToward(location: CGPoint){
        let offset = CGPoint(x: location.x - zombie.position.x, y: location.y - zombie.position.y)
        let length = sqrt(Double(offset.x * offset.x  + offset.y * offset.y))
        let direction = CGPoint(x: offset.x / CGFloat(length), y: offset.y / CGFloat(length))
        velocity = CGPoint(x:direction.x * zombieMovePointsPerSec, y:direction.y * zombieMovePointsPerSec)
    }
    
    func sceneTouched(touchLocation: CGPoint) -> Void {
        moveZombieToward(touchLocation)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        let touchLocation = touch.locationInNode(self)
        lastTouchLocation = touchLocation
        sceneTouched(touchLocation)
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        guard let touch = touches.first else{ return}
        let touchLocation = touch.locationInNode(self)
        lastTouchLocation = touchLocation
        sceneTouched(touchLocation)
    }
    
    func boundsCheckZombie() -> Void {
        let bottomLeft = CGPoint(x: 0, y: CGRectGetMinY(playableRect))
        let topLeft =  CGPoint(x: size.width, y: CGRectGetMaxY(playableRect))
        
        if zombie.position.x <= bottomLeft.x {
            zombie.position.x = bottomLeft.x
            velocity.x = -velocity.x
        }
        
        if zombie.position.x >= topLeft.x {
            zombie.position.x = topLeft.x
            velocity.x = -velocity.x
        }
        
        if zombie.position.y <= bottomLeft.y {
            zombie.position.y = bottomLeft.y
            velocity.y = -velocity.y
        }
        
        if zombie.position.y >= topLeft.y  {
            zombie.position.y = topLeft.y
            velocity.y = -velocity.y
        }
    }
    
    func debugDrawPlayableArea(){
        let shape = SKShapeNode()
        let path = CGPathCreateMutable()
        CGPathAddRect(path, nil, playableRect)
        shape.path = path
        shape.strokeColor = SKColor.redColor()
        shape.lineWidth = 4.0
        addChild(shape)
    }
    
    func rotateSprite(sprite: SKSpriteNode, direction: CGPoint){
        sprite.zRotation = CGFloat(
            atan2(Double(direction.y), Double(direction.x))
        )
    }
    
    func rotateSprite(sprite: SKSpriteNode, direction: CGPoint, rotateRadiansPerSec:CGFloat) {
        let destAngle = direction.angle
        let currentAngle = sprite.zRotation
        
    }
}

