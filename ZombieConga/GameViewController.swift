//
//  GameViewController.swift
//  ZombieConga
//
//  Created by 范武 on 16/4/6.
//  Copyright (c) 2016年 wupher. All rights reserved.
//

import UIKit
import SpriteKit

class GameViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let scene = GameScene(size: CGSize(width: 2048, height: 1536))
        let skview = self.view as! SKView
        skview.showsFPS = true
        skview.showsNodeCount = true
        skview.ignoresSiblingOrder = true
        scene.scaleMode = .AspectFill
        skview.presentScene(scene)
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
}
